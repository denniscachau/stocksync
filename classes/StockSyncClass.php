<?php
/**
 * 2007-2021 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2021 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class StockSyncClass extends ObjectModel
{
    /** @var string Name */
    public $id_product_1;
    public $id_product_attribute_1;
    public $id_product_2;
    public $id_product_attribute_2;
    public $primary;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'stocksync',
        'primary' => 'id_stocksync',
        'multilang' => false,
        'fields' => array(
            'id_product_1' => array(
                'type' => self::TYPE_INT, 'required' => true
            ),
            'id_product_attribute_1' => array(
                'type' => self::TYPE_INT, 'required' => false, 'default' => 0
            ),
            'id_product_2' => array(
                'type' => self::TYPE_STRING, 'required' => true
            ),
            'id_product_attribute_2' => array(
                'type' => self::TYPE_INT, 'required' => false, 'default' => 0
            ),
            'primary' => array(
                'type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false
            ),
        ),
    );

    public function add($auto_date = true, $null_values = false)
    {
        $this->primary = 1;
        if (!parent::add($auto_date, $null_values)) {
            return false;
        }

        // AJOUT de l'inverse
        $product_1 = $this->id_product_1;
        $product_attribute_1 = $this->id_product_attribute_1;

        $this->id_product_1 = $this->id_product_2;
        $this->id_product_attribute_1 = $this->id_product_attribute_2;
        $this->id_product_2 = $product_1;
        $this->id_product_attribute_2 = $product_attribute_1;

        $this->primary = 0;
        if (!parent::add($auto_date, $null_values)) {
            return false;
        }

        $quantity = StockAvailable::getQuantityAvailableByProduct($this->id_product_2, $this->id_product_attribute_2);

        $this->setQuantityObject($quantity);

        return true;
    }

    public function delete()
    {
        $id_product_1 = (int)$this->id_product_1;
        $id_product_attribute_1 = (int)$this->id_product_attribute_1;
        $id_product_2 = (int)$this->id_product_2;
        $id_product_attribute_2 = (int)$this->id_product_attribute_2;

        if (!parent::delete()) {
            return false;
        }

        $id_stocksync = $this->getObjectByIds(
            $id_product_2,
            $id_product_attribute_2,
            $id_product_1,
            $id_product_attribute_1
        );

        if ($id_stocksync > 0) {
            $this->id = $id_stocksync;
            $this->id_product_1 = $id_product_1;
            $this->id_product_attribute_1 = $id_product_attribute_1;
            $this->id_product_2 = $id_product_2;
            $this->id_product_attribute_2 = $id_product_attribute_2;
            $this->primary = 0;

            return parent::delete();
        }

        return true;
    }

    private function getObjectByIds($id_product_1, $id_product_attribute_1, $id_product_2, $id_product_attribute_2)
    {
        return (int)Db::getInstance()->getValue(
            "SELECT `id_stocksync`
            FROM `" . _DB_PREFIX_ . "stocksync`
            WHERE `id_product_1` = " . (int)$id_product_1 . " 
                AND `id_product_attribute_1` = " . (int)$id_product_attribute_1 . "
                AND `id_product_2` = " . (int)$id_product_2 . " 
                AND `id_product_attribute_2` = " . (int)$id_product_attribute_2 . "
                AND `primary` = 0"
        );
    }

    public static function getStockSync($id_product, $id_product_attribute)
    {
        return Db::getInstance()->getRow(
            "SELECT `id_product_2` AS 'id_product', `id_product_attribute_2` AS 'id_product_attribute'
            FROM `" . _DB_PREFIX_ . "stocksync`
            WHERE `id_product_1` = " . (int)$id_product . " 
                AND `id_product_attribute_1` = " . (int)$id_product_attribute
        );
    }

    public function setQuantityObject($quantity)
    {
        self::setQuantity($this->id_product_2, $this->id_product_attribute_2, $quantity);
    }

    public static function setQuantity($id_product, $id_product_attribute, $quantity, $id_shop = null, $add_movement = true)
    {
        if (!Validate::isUnsignedId($id_product)) {
            return false;
        }
        $context = Context::getContext();
        // if there is no $id_shop, gets the context one
        if ($id_shop === null && Shop::getContext() != Shop::CONTEXT_GROUP) {
            $id_shop = (int) $context->shop->id;
        }
        $depends_on_stock = StockAvailable::dependsOnStock($id_product);
        //Try to set available quantity if product does not depend on physical stock
        if (!$depends_on_stock) {
            $stockManager = PrestaShop\PrestaShop\Adapter\ServiceLocator::get('\\PrestaShop\\PrestaShop\\Core\\Stock\\StockManager');

            $id_stock_available = (int) StockAvailable::getStockAvailableIdByProductId($id_product, $id_product_attribute, $id_shop);
            if ($id_stock_available) {
                $stock_available = new StockAvailable($id_stock_available);

                $deltaQuantity = -1 * ((int) $stock_available->quantity - (int) $quantity);

                $stock_available->quantity = (int) $quantity;
                $stock_available->update();

                if (true === $add_movement && 0 != $deltaQuantity) {
                    $stockManager->saveMovement($id_product, $id_product_attribute, $deltaQuantity);
                }
            } else {
                $out_of_stock = StockAvailable::outOfStock($id_product, $id_shop);
                $stock_available = new StockAvailable();
                $stock_available->out_of_stock = (int) $out_of_stock;
                $stock_available->id_product = (int) $id_product;
                $stock_available->id_product_attribute = (int) $id_product_attribute;
                $stock_available->quantity = (int) $quantity;
                if ($id_shop === null) {
                    $shop_group = Shop::getContextShopGroup();
                } else {
                    $shop_group = new ShopGroup((int) Shop::getGroupFromShop((int) $id_shop));
                }
                // if quantities are shared between shops of the group
                if ($shop_group->share_stock) {
                    $stock_available->id_shop = 0;
                    $stock_available->id_shop_group = (int) $shop_group->id;
                } else {
                    $stock_available->id_shop = (int) $id_shop;
                    $stock_available->id_shop_group = 0;
                }
                $stock_available->add();

                if (true === $add_movement && 0 != $quantity) {
                    $stockManager->saveMovement($id_product, $id_product_attribute, (int) $quantity);
                }
            }
        }
        Cache::clean('StockAvailable::getQuantityAvailableByProduct_' . (int) $id_product . '*');
    }

    public static function getOutOfStock($id_product, $id_product_attribute = 0, $id_shop = null)
    {
        if (!Validate::isUnsignedId($id_product) || !Validate::isUnsignedId($id_product_attribute)) {
            return false;
        }

        $query = new DbQuery();
        $query->select('out_of_stock');
        $query->from('stock_available');
        $query->where('id_product = ' . (int) $id_product);
        $query->where('id_product_attribute = ' . (int)$id_product_attribute);

        $query = StockAvailable::addSqlShopRestriction($query, $id_shop);

        return (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
    }

    public static function isProductAlreadySync($id_product, $id_product_attribute = 0)
    {
        $query = "SELECT `id_stocksync` 
            FROM `" . _DB_PREFIX_ . "stocksync` 
            WHERE (`id_product_1` = " . (int)$id_product . " 
                AND `id_product_attribute_1` = " . (int)$id_product_attribute . ") 
                OR (`id_product_2` = " . (int)$id_product . " 
                AND `id_product_attribute_2` = " . (int)$id_product_attribute . ")";

        return (bool)Db::getInstance()->getValue($query);
    }

    public static function getCombinationsForProduct($id_product, Context $context = null)
    {
        $product = new Product($id_product);

        if ($context === null) {
            $context = Context::getContext();
        }

        if (Validate::isLoadedObject($product)) {
            $sql = 'SELECT pa.*, product_attribute_shop.*, ag.`id_attribute_group`, ag.`is_color_group`, agl.`name` AS group_name, GROUP_CONCAT(DISTINCT al.`name` SEPARATOR ", ") AS attribute_name,
                    a.`id_attribute`
                FROM `' . _DB_PREFIX_ . 'product_attribute` pa
                ' . Shop::addSqlAssociation('product_attribute', 'pa') . '
                LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_combination` pac ON pac.`id_product_attribute` = pa.`id_product_attribute`
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute` a ON a.`id_attribute` = pac.`id_attribute`
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group` ag ON ag.`id_attribute_group` = a.`id_attribute_group`
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = ' . (int) $context->language->id . ')
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = ' . (int) $context->language->id . ')
                WHERE pa.`id_product` = ' . (int) $product->id . '
                GROUP BY pa.`id_product_attribute`
                ORDER BY pa.`id_product_attribute`';

            // $product_attributes = $product->getAttributeCombinations($this->context->language->id, false);

            return Db::getInstance()->executeS($sql);
        } else {
            return array();
        }
    }
}

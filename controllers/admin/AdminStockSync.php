<?php
/**
 * 2007-2021 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2021 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

include_once _PS_MODULE_DIR_ . 'stocksync/classes/StockSyncClass.php';

class AdminStockSyncController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'stocksync';
        $this->identifier = 'id_stocksync';
        $this->className = 'StockSyncClass';
        $this->tabClassName = 'AdminStockSync';
        $this->lang = false;
        $this->deleted = false;
        $this->colorOnBackground = false;
        $this->bulk_actions = array(
            'delete' => array('text' => 'Delete selected', 'confirm' => 'Delete selected items?'),
        );
        $this->context = Context::getContext();

        parent::__construct();

        $this->fields_list = array(
            'id_stocksync' => array(
                'title' => $this->l('ID'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
            'p1' => array(
                'title' => $this->l('Produit 1'),
                'align' => 'text-center',
            ),
            'p2' => array(
                'title' => $this->l('Produit 2'),
                'align' => 'text-center',
            ),
        );

        $this->_select = "CONCAT(pl1.`name`, IF(`id_product_attribute_1` > 0, CONCAT(' : ', GROUP_CONCAT(DISTINCT al1.`name` SEPARATOR ', ')), '')) AS 'p1'";
        $this->_select .= ", CONCAT(pl2.`name`, IF(`id_product_attribute_2` > 0, CONCAT(' : ', GROUP_CONCAT(DISTINCT al2.`name` SEPARATOR ', ')), '')) AS 'p2'";

        $id_lang = (int)$this->context->language->id;
        $this->_join = " LEFT JOIN `" . _DB_PREFIX_ . "product_lang` pl1
            ON a.`id_product_1` = pl1.`id_product` AND pl1.`id_lang` = " . $id_lang;
        $this->_join .= " LEFT JOIN `" . _DB_PREFIX_ . "product_attribute_combination` pac1 
            ON a.`id_product_attribute_1` = pac1.`id_product_attribute`";
        $this->_join .= " LEFT JOIN `" . _DB_PREFIX_ . "attribute` a1 
            ON pac1.`id_attribute` = a1.`id_attribute`";
        $this->_join .= " LEFT JOIN `" . _DB_PREFIX_ . "attribute_lang` al1
            ON a1.`id_attribute` = al1.`id_attribute` AND al1.`id_lang` = " . $id_lang;

        $this->_join .= " LEFT JOIN `" . _DB_PREFIX_ . "product_lang` pl2
            ON a.`id_product_2` = pl2.`id_product` AND pl2.`id_lang` = " . $id_lang;
        $this->_join .= " LEFT JOIN `" . _DB_PREFIX_ . "product_attribute_combination` pac2 
            ON a.`id_product_attribute_2` = pac2.`id_product_attribute`";
        $this->_join .= " LEFT JOIN `" . _DB_PREFIX_ . "attribute` a2 
            ON pac2.`id_attribute` = a2.`id_attribute`";
        $this->_join .= " LEFT JOIN `" . _DB_PREFIX_ . "attribute_lang` al2
            ON a2.`id_attribute` = al2.`id_attribute` AND al2.`id_lang` = " . $id_lang;

        $this->_where = " AND `primary` = '1'";

        $this->_group = "GROUP BY `id_stocksync`";
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();

        if (empty($this->display)) {
            $this->page_header_toolbar_btn['desc-module-new'] = array(
                'href' => 'index.php?controller=' . $this->tabClassName . '&add' . $this->table . '&token=' .
                    Tools::getAdminTokenLite($this->tabClassName),
                'desc' => $this->l('Add a new column'),
                'icon' => 'process-icon-new',
            );
        }
    }

    public function renderList()
    {
        $this->addRowAction('delete');

        $this->informations[] =
            $this->l('Les stocks des produits sont liés dans les deux sens. (Produit 1 -> Produit 2 ; Produit 2 -> Produit 1)');
        $this->informations[] =
            $this->l('Un produit ne peut pas être lié à plus d\'un produit');

        $script = $this->context->smarty->fetch($this->getTemplatePath() . "renderList.tpl");

        return parent::renderList() . $script;
    }

    public function renderForm()
    {
        $products = Product::getProducts($this->context->language->id, 0, 0, "id_product", "desc");

        $products = array_map(function ($row) {
            $row['name'] = $row['id_product'] . ' - ' . $row['name'];
            return $row;
        }, $products);

        $products = array_merge(
            array(array('id_product' => 0, 'name' => $this->l('Sélectionnez un produit'))),
            $products
        );

        $products_attribute_1 = array();
        if (Tools::getIsset('id_product_1')) {
            $products_attribute_1 = StockSyncClass::getCombinationsForProduct(
                Tools::getValue('id_product_1'),
                $this->context
            );
        }
        if (!empty($products_attribute_1)) {
            $products_attribute_1 = array_merge(
                array(array('id_product_attribute' => 0, 'attribute_name' => $this->l('Sélectionnez une déclinaison'))),
                $products_attribute_1
            );
        } else {
            $products_attribute_1 = array(array(
                'id_product_attribute' => 0, 'attribute_name' => $this->l('Pas de déclinaison disponible')
            ));
        }

        $products_attribute_2 = array();
        if (Tools::getIsset('id_product_2')) {
            $products_attribute_2 = StockSyncClass::getCombinationsForProduct(
                Tools::getValue('id_product_2'),
                $this->context
            );
        }
        if (!empty($products_attribute_2)) {
            $products_attribute_2 = array_merge(
                array(array('id_product_attribute' => 0, 'attribute_name' => $this->l('Sélectionnez une déclinaison'))),
                $products_attribute_2
            );
        } else {
            $products_attribute_2 = array(array(
                'id_product_attribute' => 0, 'attribute_name' => $this->l('Pas de déclinaison disponible')
            ));
        }

        $this->fields_form = [
            'legend' => [
                'title' => $this->l('Lier le stock de 2 produits'),
                'icon' => 'icon-info-sign',
            ],
            'input' => [
                [
                    'type' => 'select',
                    'label' => $this->l('Produit 1'),
                    'name' => 'id_product_1',
                    'required' => true,
                    'options' => [
                        'query' => $products,
                        'id' => 'id_product',
                        'name' => 'name',
                    ],
                    'class' => 'ss_products',
//                    'form_group_class' => 'col-md-6'
                ],
                [
                    'type' => 'select',
                    'label' => $this->l('Déclinaison 1'),
                    'name' => 'id_product_attribute_1',
                    'required' => false,
                    'options' => [
                        'query' => $products_attribute_1,
                        'id' => 'id_product_attribute',
                        'name' => 'attribute_name',
                    ],
                    'disabled' => count($products_attribute_1) <= 1,
//                    'form_group_class' => 'col-md-6'
                ],
                [
                    'type' => 'html',
                    'name' => 'html_content',
                    'html_content' => '<hr>'
                ],
                [
                    'type' => 'select',
                    'label' => $this->l('Produit 2'),
                    'name' => 'id_product_2',
                    'required' => true,
                    'options' => [
                        'query' => $products,
                        'id' => 'id_product',
                        'name' => 'name',
                    ],
                    'class' => 'ss_products',
//                    'form_group_class' => 'col-md-6'
                ],
                [
                    'type' => 'select',
                    'label' => $this->l('Déclinaison 2'),
                    'name' => 'id_product_attribute_2',
                    'required' => false,
                    'options' => [
                        'query' => $products_attribute_2,
                        'id' => 'id_product_attribute',
                        'name' => 'attribute_name',
                    ],
                    'disabled' => count($products_attribute_2) <= 1,
//                    'form_group_class' => 'col-md-6'
                ],
            ],
            'submit' => [
                'title' => $this->l('Enregistrer')
            ]
        ];

        $this->context->smarty->assign(array(
            'link' => $this->context->link
        ));

        $configure = $this->context->smarty->fetch($this->getTemplatePath() . "configure.tpl");

        $this->informations[] =
            $this->l('La quantité du produit 1 sera la quantité prise en compte pour les deux produits.');

        return parent::renderForm() . $configure;
    }

    public function ajaxProcessGetProductAttributes()
    {
        $id_product = (int)Tools::getValue('id_product');
        die(json_encode(StockSyncClass::getCombinationsForProduct($id_product, $this->context)));
    }

    public function initProcess()
    {
        if (Tools::isSubmit('submitAddstocksync')) {
            $id_product = (int)Tools::getValue('id_product_1');
            $id_product_attribute = (int)Tools::getValue('id_product_attribute_1');
            $id_product_sync = (int)Tools::getValue('id_product_2');
            $id_product_attribute_sync = (int)Tools::getValue('id_product_attribute_2');

            if ($id_product <= 0 || $id_product_sync <= 0) {
                $this->errors[] = $this->l('Veuillez sélectionner un produit 1 et 2.');
            }
            if ($id_product == $id_product_sync) {
                $this->errors[] = $this->l('Veuillez sélectionner deux produits différents pour la liaison.');
            }

            if (StockSyncClass::isProductAlreadySync($id_product, $id_product_attribute)) {
                $this->errors[] = sprintf(
                    $this->l('Le produit ayant pour ID %d est déjà lié à un autre produit'),
                    $id_product
                );
            } elseif (StockSyncClass::isProductAlreadySync($id_product_sync, $id_product_attribute_sync)) {
                $this->errors[] = sprintf(
                    $this->l('Le produit ayant pour ID %d est déjà lié à un autre produit'),
                    $id_product_sync
                );
            }

            if (empty($this->errors)) {
                $product = new Product($id_product);
                $product_sync = new Product($id_product_sync);

                if (Validate::isLoadedObject($product) && Validate::isLoadedObject($product_sync)) {
                    if ($id_product_attribute == 0 && $product->hasCombinations()) {
                        $this->errors[] = sprintf(
                            $this->l('Veuillez sélectionner une déclinaison pour le produit %d.'),
                            $id_product
                        );
                    }
                    if ($id_product_attribute_sync == 0 && $product_sync->hasCombinations()) {
                        $this->errors[] = sprintf(
                            $this->l('Veuillez sélectionner une déclinaison pour le produit %d.'),
                            $id_product_sync
                        );
                    }
                } else {
                    $this->errors[] = $this->l('Veuillez sélectionner deux produits existants.');
                }
            }
        }

        parent::initProcess();

        if ($this->display == 'edit') {
            $link = $this->context->link->getAdminLink('AdminStockSync');
            Tools::redirectAdmin($link);
        }
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitAddstocksync')) {
            $id_product = (int)Tools::getValue('id_product_1');
            $id_product_attribute = (int)Tools::getValue('id_product_attribute_1');
            $id_product_sync = (int)Tools::getValue('id_product_2');
            $id_product_attribute_sync = (int)Tools::getValue('id_product_attribute_2');

            $quantity = StockAvailable::getQuantityAvailableByProduct($id_product, $id_product_attribute);

            StockAvailable::setQuantity(
                $id_product_sync,
                $id_product_attribute_sync,
                $quantity,
                null,
                false
            );

            $out_of_stock = StockSyncClass::getOutOfStock(
                $id_product,
                $id_product_attribute
            );
            if ($out_of_stock !== false) {
                StockAvailable::setProductOutOfStock(
                    $id_product_sync,
                    (int)$out_of_stock,
                    null,
                    $id_product_attribute_sync
                );
            }

            $location = StockAvailable::getLocation($id_product, $id_product_attribute);
            StockAvailable::setLocation($id_product_sync, $location, null, $id_product_attribute_sync);
        }

        return parent::postProcess();
    }
}

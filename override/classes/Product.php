<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

class Product extends ProductCore
{
    /**
     * Get available product quantities (this method already have decreased products in cart).
     *
     * @param int $idProduct Product id
     * @param int|null $idProductAttribute Product attribute id (optional)
     * @param bool|null $cacheIsPack
     * @param Cart|null $cart
     * @param int|null $idCustomization Product customization id (optional)
     *
     * @return int Available quantities
     */
    public static function getQuantity(
        $idProduct,
        $idProductAttribute = null,
        $cacheIsPack = null,
        Cart $cart = null,
        $idCustomization = null
    ) {
        // pack usecase: Pack::getQuantity() returns the pack quantity after cart quantities have been removed from stock
        if (Pack::isPack((int) $idProduct)) {
            return Pack::getQuantity($idProduct, $idProductAttribute, $cacheIsPack, $cart, $idCustomization);
        }
        $availableQuantity = StockAvailable::getQuantityAvailableByProduct($idProduct, $idProductAttribute);
        $nbProductInCart = 0;
        $nbProductSyncInCart = 0;

        // we don't substract products in cart if the cart is already attached to an order, since stock quantity
        // has already been updated, this is only useful when the order has not yet been created
        if (!empty($cart) && empty(Order::getByCartId($cart->id))) {
            $cartProduct = $cart->getProductQuantity($idProduct, $idProductAttribute, $idCustomization);

            if (!empty($cartProduct['deep_quantity'])) {
                $nbProductInCart = $cartProduct['deep_quantity'];
            }

            include_once _PS_MODULE_DIR_ . 'stocksync/classes/StockSyncClass.php';
            $stock_sync = StockSyncClass::getStockSync((int)$idProduct, (int)$idProductAttribute);

            if ($stock_sync) {
                $cartProductSync = $cart->getProductQuantity(
                    (int)$stock_sync['id_product'],
                    (int)$stock_sync['id_product_attribute']
                );

                if (!empty($cartProductSync['deep_quantity'])) {
                    $nbProductSyncInCart = $cartProductSync['deep_quantity'];
                }
            }
        }

        // @since 1.5.0
        return $availableQuantity - $nbProductInCart - $nbProductSyncInCart;
    }
}

{*
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script>
	$('document').ready(() => {
		$('.ss_products').on('change', function() {
			let ajax_url = "{$link->getAdminLink('AdminStockSync')|escape:'javascript':'UTF-8'}";
			let id_product = $(this).val();
			let body = {
				id_product: id_product,
				controller: 'AdminStockSync',
				action: 'getProductAttributes',
				ajax: true
			};
			let product_attribute_input = $(this).parents('.form-group').next().find('select');

			if (product_attribute_input) {
				$(product_attribute_input).val(0);
				$(product_attribute_input).find('option').remove();
				$(product_attribute_input).attr('disabled', 'disabled');
			}

			$.post(ajax_url, body, (product_attributes) => {
				product_attributes = JSON.parse(product_attributes);

				let select_values = [];
				$.each(product_attributes, (i, v) => {
					select_values.push('<option value="'+v.id_product_attribute+'">'+v.attribute_name+'</option>')
				});
				if (select_values.length > 0) {
					select_values.unshift('<option value="0">Sélectionnez une déclinaison</option>')

					product_attribute_input.html(select_values.join(''));
					$(product_attribute_input).removeAttr('disabled').attr('required', 'required')
				} else {
					product_attribute_input.html('<option value="0">Pas de déclinaison disponible</option>');
					$(product_attribute_input).removeAttr('required')
				}
			})
		})
	})
</script>

<?php
/**
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once _PS_MODULE_DIR_ . 'stocksync/classes/StockSyncClass.php';

class Stocksync extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'stocksync';
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.0';
        $this->author = 'Ciren';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Synchroniser les stocks entre deux produits');
        $this->description = $this->l('Synchroniser les stocks entre deux produits');

        $this->ps_versions_compliancy = array('min' => '1.7.7.0', 'max' => '1.7.7.99');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (!parent::install()) {
            return false;
        }

        include(dirname(__FILE__).'/sql/install.php');

        return $this->addTab() && $this->registerHook('actionUpdateQuantity');
    }

    private function addTab()
    {
        if (Tab::getIdFromClassName('AdminStockSync') === false) {
            $tab = new Tab();
            $tab->class_name = 'AdminStockSync';
            $tab->module = $this->name;
            $tab->id_parent = (int)Tab::getIdFromClassName('AdminCatalog');
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Fusionner des stocks');
            $tab->add();
        }

        return true;
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $link = new Link();
        $link = $link->getAdminLink('AdminStockSync', true);
        Tools::redirectAdmin($link);
    }

    public function hookActionUpdateQuantity($params)
    {
        $id_product = (int)$params['id_product'];
        $id_product_attribute = (int)$params['id_product_attribute'];
        $stock_sync = StockSyncClass::getStockSync($id_product, $id_product_attribute);

        if ($stock_sync) {
            $quantity = (int)$params['quantity'];
            $id_product_sync = (int)$stock_sync['id_product'];
            $id_product_attribute_sync = (int)$stock_sync['id_product_attribute'];

            StockSyncClass::setQuantity(
                $id_product_sync,
                $id_product_attribute_sync,
                $quantity
            );
        }
    }
}
